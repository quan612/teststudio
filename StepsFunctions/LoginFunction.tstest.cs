using Telerik.TestStudio.Translators.Common;
using Telerik.TestingFramework.Controls.TelerikUI.Blazor;
using Telerik.TestingFramework.Controls.KendoUI.Angular;
using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Windows;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace TestProject1
{

    public class LoginFunction : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        public static string UserName {get; set;}
        public static string Password {get; set;}
        [CodedStep(@"Enter text 'automationqa' in 'UsernameText'")]
        public void Login_CodedStep()
        {
            // Enter text 'automationqa' in 'UsernameText'
            Actions.SetText(Pages.QaRegressionSignIn.UsernameText, "");
            Pages.QaRegressionSignIn.UsernameText.ScrollToVisible(ArtOfTest.WebAii.Core.ScrollToVisibleType.ElementCenterAtWindowCenter);
            ActiveBrowser.Window.SetFocus();
            
            ClearInput(Pages.QaRegressionSignIn.UsernameText);
            Manager.Desktop.KeyBoard.TypeText(UserName, 50, 100, true);
            
        }
    
        [CodedStep(@"Enter text 'AthenaAlexFoo' in 'PasswordPassword'")]
        public void Login_CodedStep1()
        {
            // Enter text 'AthenaAlexFoo' in 'PasswordPassword'
            Actions.SetText(Pages.QaRegressionSignIn.PasswordPassword, "");
            Pages.QaRegressionSignIn.PasswordPassword.ScrollToVisible(ArtOfTest.WebAii.Core.ScrollToVisibleType.ElementCenterAtWindowCenter);
            ActiveBrowser.Window.SetFocus();
            Pages.QaRegressionSignIn.PasswordPassword.Focus();
            Pages.QaRegressionSignIn.PasswordPassword.MouseClick();
            Manager.Desktop.KeyBoard.TypeText(Password, 50, 100, true);
            
        }
        
        private void ClearInput(HtmlInputText input){
            input.Focus();
            input.MouseClick();
            input.Text="";
//            Manager.Desktop.KeyBoard.KeyDown(System.Windows.Forms.Keys.Control); //hold control key
//            Manager.Desktop.KeyBoard.KeyPress(System.Windows.Forms.Keys.A); //press A to select all text
//            Manager.Desktop.KeyBoard.KeyUp(System.Windows.Forms.Keys.Control); //let go of control
//            Manager.Desktop.KeyBoard.KeyPress(System.Windows.Forms.Keys.Delete);
        }
    }
}
