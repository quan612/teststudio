using Telerik.TestStudio.Translators.Common;
using Telerik.TestingFramework.Controls.TelerikUI.Blazor;
using Telerik.TestingFramework.Controls.KendoUI.Angular;
using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace TestProject1
{

    public class LoginTestF : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        [CodedStep(@"WrongUsername_WrongPassword - expect to see 'Invalid username or password.'")]
        public void WrongUsername_WrongPassword_CodedStep()
        {
            LoginFunction.UserName="quan123";
            LoginFunction.Password="Qu@naaa";
            this.ExecuteTest("StepsFunctions\\LoginFunction.tstest");       
        }
    
        [CodedStep(@"Verify 'TextContent' 'Contains' 'Invalid username or password.' on 'SignIn_InvalidErrorSpan'")]
        public void LoginTestF_CodedStep()
        {
            // Verify 'TextContent' 'Contains' 'Invalid username or password.' on 'SignIn_InvalidErrorSpan'
            Pages.QaLovelandSignIn.SignIn_InvalidErrorSpan.AssertContent().TextContent(ArtOfTest.Common.StringCompareType.Contains, "Invalid username or password.");
            
        }
    
        [CodedStep(@"Verify element 'SignIn_InvalidErrorSpan' 'is' visible.")]
        public void LoginTestF_CodedStep2()
        {
            // Verify element 'SignIn_InvalidErrorSpan' 'is' visible.
            Pages.QaLovelandSignIn.SignIn_InvalidErrorSpan.Wait.ForExists(30000);
            Assert.AreEqual(true, Pages.QaLovelandSignIn.SignIn_InvalidErrorSpan.IsVisible());
            
        }
    
        [CodedStep(@"Execute test 'LoginFunction'")]
        public void CorrectUsername_CorrectPassword_CodedStep3()
        {
            LoginFunction.UserName="automationqa";
            LoginFunction.Password="AthenaAlexFoo";
            this.ExecuteTest("StepsFunctions\\LoginFunction.tstest");
            
        }
    
        [CodedStep(@"Navigate to : 'http://192.168.1.35/Account/SignIn'")]
        public void LoginTestF_CodedStep1()
        {
            // Navigate to : 'http://192.168.1.35/Account/SignIn'
            ActiveBrowser.NavigateTo("http://192.168.1.35/Account/SignIn", true);
            
        }
    }
}
